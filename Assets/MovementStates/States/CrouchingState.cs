using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrouchingState : MovementBaseState
{
    public override void EnterState(MovementStateManager movement)
    {
        movement.anim.SetBool("Crouching", true);
    }

    public override void UpdateState(MovementStateManager movement)
    {
        if (Input.GetKey(KeyCode.LeftShift)) ExitSate(movement, movement.Running);
        if (Input.GetKeyDown(KeyCode.C)) 
        {
          if (movement.dir.magnitude < 0.1f) ExitSate(movement, movement.Idle);
          else ExitSate(movement, movement.Walking);

        }

        if (movement.vInput < 0) movement.CurrentMoveSpeed = movement.crouchBackSpeed;
        else movement.CurrentMoveSpeed = movement.crouchSpeed;
    }
    void ExitSate(MovementStateManager movement, MovementBaseState state)
    {
        movement.anim.SetBool("Crouching", false);
        movement.SwitchState(state);
    }
}
