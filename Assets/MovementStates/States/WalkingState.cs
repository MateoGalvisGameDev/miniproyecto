using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingState : MovementBaseState
{
    public override void EnterState(MovementStateManager movement)
    {

        movement.anim.SetBool("Walking",true);
    }

    public override void UpdateState(MovementStateManager movement)
    {
        if (Input.GetKey(KeyCode.LeftShift)) ExitSate(movement,movement.Running);
        else if (Input.GetKeyDown(KeyCode.C)) ExitSate(movement,movement.Crouching);
        else if (movement.dir.magnitude < 0.1f) ExitSate(movement,movement.Idle);

        if (movement.vInput < 0) movement.CurrentMoveSpeed = movement.walkBackSpeed;
        else movement.CurrentMoveSpeed = movement.walkSpeed;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            movement.previousState = this;
            ExitSate(movement,movement.Jumping);
        }
    }

    void ExitSate(MovementStateManager movement,MovementBaseState state)
    {
        movement.anim.SetBool("Walking", false);
        movement.SwitchState(state);
    }
}
