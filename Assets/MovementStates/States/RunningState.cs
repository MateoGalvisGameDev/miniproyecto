using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningState :MovementBaseState
{
    public override void EnterState(MovementStateManager movement)
    {
        movement.anim.SetBool("Running", true);
    }

    public override void UpdateState(MovementStateManager movement)
    {
        if (Input.GetKeyUp(KeyCode.LeftShift)) ExitSate(movement, movement.Walking);
        else if (movement.dir.magnitude < 0.1f) ExitSate(movement, movement.Idle);

        if (movement.vInput < 0) movement.CurrentMoveSpeed = movement.runBackSpeed;
        else movement.CurrentMoveSpeed = movement.runSpeed;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            movement.previousState = this;
            ExitSate(movement, movement.Jumping);
        }
    }
    void ExitSate(MovementStateManager movement, MovementBaseState state)
    {
        movement.anim.SetBool("Running", false);
        movement.SwitchState(state);
    }
}
