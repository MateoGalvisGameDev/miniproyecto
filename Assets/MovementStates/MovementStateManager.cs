
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementStateManager : MonoBehaviour
{
    ////Estados
    public MovementBaseState previousState;
    public MovementBaseState currentState;

    public IdleState Idle=new IdleState();
    public WalkingState Walking=new WalkingState();
    public RunningState Running=new RunningState();
    public CrouchingState Crouching=new CrouchingState();
    public JumpState Jumping=new JumpState();
    ////
    
    [HideInInspector] public Animator anim;


    public float CurrentMoveSpeed;
    public float walkSpeed=3,walkBackSpeed=2;
    public float runSpeed=7,runBackSpeed=5;
    public float crouchSpeed=2,crouchBackSpeed = 1;

    [HideInInspector] public Vector3 dir;
    [HideInInspector] public float hzInput, vInput;
    public CharacterController characterController;

    ///Suelo
    [SerializeField] float groundYOffset;
    [SerializeField]LayerMask groundMask;
    Vector3 spherePosition;
    ///

    //Gravedad
    [SerializeField] float gravity=-9.81f;
    [SerializeField] float jumpForce;
    [HideInInspector] public bool jumped;
    Vector3 velocity;
    //

    void Start()
    {
        anim =GetComponentInChildren<Animator>();
        characterController= GetComponent<CharacterController>();
        SwitchState(Idle);
    }

  
    void Update()
    {
        getDirectionsAndMove();
        Gravity();

        anim.SetFloat("hzInput", hzInput);
        anim.SetFloat("vInput", vInput);

        currentState.UpdateState(this);


    }

    public void SwitchState(MovementBaseState state)
    {
        currentState= state;
        currentState.EnterState(this);
    }
    void getDirectionsAndMove()
    {
        hzInput = Input.GetAxis("Horizontal");
        vInput = Input.GetAxis("Vertical");

        dir=transform.forward * vInput + transform.right*hzInput;

        characterController.Move(dir.normalized*CurrentMoveSpeed*Time.deltaTime);
    }

   public bool IsGrounded()
    {
        spherePosition = new Vector3(transform.position.x, transform.position.y - groundYOffset, transform.position.z);
        if (Physics.CheckSphere(spherePosition, characterController.radius - 0.05f, groundMask)) return true;
        return false;
    }

    public void Gravity()
    {
        if (!IsGrounded()) velocity.y += gravity * Time.deltaTime;
       
        else if(velocity.y < 0) velocity.y = -2;

        characterController.Move(velocity * Time.deltaTime);
    }
    public void JumpForce() => velocity.y += jumpForce;
    public void Jumped() => jumped = true;

}
