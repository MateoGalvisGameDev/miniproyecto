
using System.Collections;

using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public static UIController instance; // Singleton pattern

    public Image crosshair; // Aseg�rate de asignar esto en el inspector

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);


    }

    public void ChangeCrosshairColor(Color color)
    {
        crosshair.color = color;

    }

    public IEnumerator TurnWhite()
    {
        yield return new WaitForSeconds(5);
        crosshair.color = Color.white;
    }
}
