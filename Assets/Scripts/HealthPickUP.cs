using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPickUP : MonoBehaviour
{
    public int healAmount = 10; // Cantidad de vida que se recupera al recoger el objeto
    public float rotationSpeed = 50f; // Velocidad de rotaci�n del PickUp
    public AudioClip pickupSound; // Sonido al recoger el PickUp
    public AudioSource audioSource;

    private void Update()
    {
        // Rotaci�n del PickUp
        transform.Rotate(Vector3.forward * rotationSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        // Verifica si el objeto que entra en el collider es el jugador
        if (other.CompareTag("Player"))
        {
            // Obt�n el componente PlayerHealth del jugador
            PlayerHealth playerHealth = other.GetComponent<PlayerHealth>();

            // Verifica si el jugador no est� al m�ximo de salud
            if (playerHealth != null && playerHealth.currentHealth < playerHealth.maxHealth)
            {
                // Recupera la vida del jugador
                playerHealth.currentHealth = Mathf.Min(playerHealth.maxHealth, playerHealth.currentHealth + healAmount);
                playerHealth.healthBar.slider.value = playerHealth.currentHealth; // Actualiza la barra de vida

                // Reproduce el sonido del PickUp
                if (audioSource != null && pickupSound != null)
                {
                    audioSource.PlayOneShot(pickupSound);
                }

                // Desactiva el objeto del PickUp
                Destroy(this.gameObject);

                // Puedes destruir el objeto si prefieres
                // Destroy(gameObject);
            }
        }
    }
}
