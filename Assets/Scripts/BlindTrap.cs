using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlindTrap : MonoBehaviour
{
    public float duracionCeguera = 3f; // Duraci�n en segundos de la ceguera
    public AudioClip sonidoCeguera; // Sonido al activar la trampa
    public Light luzParpadeante; // Luz que parpadea durante la ceguera
    public Canvas canvasCeguera; // Canvas que cubre la pantalla durante la ceguera
    private bool jugadorEnTrampa = false;

    private void Start()
    {
        canvasCeguera.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !jugadorEnTrampa)
        {
            jugadorEnTrampa = true;
            ReproducirSonido();
            StartCoroutine(CegarJugador());
        }
    }

    void ReproducirSonido()
    {
        if (sonidoCeguera != null)
        {
            AudioSource.PlayClipAtPoint(sonidoCeguera, transform.position);
        }
    }

    IEnumerator CegarJugador()
    {
        // Activar el Canvas de ceguera
        if (canvasCeguera != null)
        {
            canvasCeguera.enabled = true;
        }

        // Realizar acciones visuales adicionales para indicar que el jugador est� cegado

        // Activar la luz que parpadea
        if (luzParpadeante != null)
        {
            StartCoroutine(ParpadearLuz());
        }

        // Desactivar el renderizado del jugador u otras acciones para simular la ceguera

        yield return new WaitForSeconds(duracionCeguera);

        // Restaurar acciones o visuales despu�s de que la ceguera haya terminado

        // Desactivar el Canvas de ceguera
        if (canvasCeguera != null)
        {
            canvasCeguera.enabled = false;
        }

        // Destruir la trampa
        Destroy(gameObject);
    }

    IEnumerator ParpadearLuz()
    {
        while (jugadorEnTrampa)
        {
            luzParpadeante.enabled = !luzParpadeante.enabled;
            yield return new WaitForSeconds(0.5f); // Ajusta el tiempo de parpadeo seg�n tus preferencias
        }
    }
}
