using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Puerta : MonoBehaviour
{

    private Animator animator;
    private bool jugadorCerca;
    public AudioClip sonido;
    public AudioSource sonidoAudioSource;
    public Canvas Boton;
    bool puertaAbierta;

    void Start()
    {
        jugadorCerca = false;
        puertaAbierta = false;
        Boton.enabled = false;
        animator = GetComponent<Animator>();
    }

    void OnTriggerEnter(Collider other)
    {
        // Verifica si el objeto que entra al collider es el jugador
        if (other.CompareTag("Player"))
        {
            jugadorCerca = true;
            Boton.enabled = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        // Verifica si el objeto que sale del collider es el jugador
        if (other.CompareTag("Player"))
        {
            jugadorCerca = false;
            Boton.enabled = false;
        }
    }

    void Update()
    {
        // Revisa la entrada del teclado en cada fotograma
        if (jugadorCerca && Input.GetKeyDown(KeyCode.E) && !puertaAbierta)
        {
            AbrirPuerta();
        }
        else if (!jugadorCerca && puertaAbierta)
        {
            CerrarPuerta();
        }
    }

    void AbrirPuerta()
    {
        puertaAbierta = true;
        sonidoAudioSource.PlayOneShot(sonido);
        animator.SetBool("Abrir", true);
        Debug.Log("Abrir Puerta");
        // Puedes reproducir el sonido aqu� si es necesario
    }

    void CerrarPuerta()
    {
        puertaAbierta = false;
        sonidoAudioSource.PlayOneShot(sonido);
        animator.SetBool("Abrir", false);
        Debug.Log("Cerrar Puerta");
        // Puedes reproducir el sonido aqu� si es necesario
    }
}
