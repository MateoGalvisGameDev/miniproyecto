using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneWithButton : MonoBehaviour
{
    private void Start()
    {
        Cursor.visible = true;
    }
    public void ChangeScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
        Debug.Log("Escena cambiada a " + SceneName);
    }

    public void Quit()
    {
     Application.Quit();
    }
}

