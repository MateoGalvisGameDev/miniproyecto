using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FINALLEVELMANAGER : MonoBehaviour
{
    
    [SerializeField] int enemyCount;
    [SerializeField] GameObject enemyPrefab;
    [SerializeField] Transform spawnPosition;

  

 

    public void EnemyDefeated()
    {
        enemyCount--;

        if (enemyCount <= 0)
        {
           
            SpawnEnemy();
        }
    }

   



    void SpawnEnemy()
    {
        if (enemyPrefab != null && spawnPosition != null)
        {
            Instantiate(enemyPrefab, spawnPosition.position, spawnPosition.rotation);
            enemyCount++; // Aumenta el contador de enemigos despu�s de spawnear uno nuevo
        }
    }
}
