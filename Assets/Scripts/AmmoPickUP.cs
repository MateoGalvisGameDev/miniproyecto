using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickUP : MonoBehaviour
{
    public int ammoAmount = 15; // Cantidad de munici�n que proporciona el PickUp
    public float rotationSpeed = 50f; // Velocidad de rotaci�n del PickUp
    public AudioClip pickupSound; // Sonido al recoger el PickUp
    public AudioSource audioSource;
    public WeaponAmmo weaponAmmo;
    public WeaponAmmo weaponAmmo2;

    private void Update()
    {
        // Rotaci�n del PickUp
        transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
    }


    private void OnTriggerEnter(Collider other)
    {
        // Verifica si el objeto que entra en el collider es el jugador
        if (other.CompareTag("Player"))
        {
            // Obt�n el componente WeaponAmmo del jugador
          

            // Verifica si se encuentra el componente
            if (weaponAmmo != null)
            {
                // A�ade munici�n al jugador
                weaponAmmo.extraAmmo += ammoAmount;
                weaponAmmo2.extraAmmo += ammoAmount;


                // Reproduce el sonido del PickUp
                if (audioSource != null && pickupSound != null)
                {
                    audioSource.PlayOneShot(pickupSound);
                }

        

                // Puedes destruir el objeto si prefieres
                Destroy(gameObject);
            }
        }
    }
}
