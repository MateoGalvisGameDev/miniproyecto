using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class BulletTimeManager : MonoBehaviour
{
    public float normalTimeScale = 1.0f;
    public float bulletTimeScale = 0.1f;
    public float bulletTimeDuration = 3.0f;
    public float rechargeRate = 1.0f; // Tasa de recarga del Bullet Time

    private bool isBulletTimeActive = false;
    private float bulletTimeCounter;
    public GameObject Crossair;
    public GameObject CrossAirAim;
    public Slider bulletTimeSlider;

    private void Start()
    {
        Crossair.SetActive(true);
        CrossAirAim.SetActive(false);
        bulletTimeSlider.minValue = 0;
        bulletTimeSlider.maxValue = bulletTimeDuration;
        bulletTimeSlider.value = bulletTimeDuration;
    }

    void Update()
    {
        // Activar Bullet Time con clic derecho
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            StartBulletTime();
            Crossair.SetActive(false);
            CrossAirAim.SetActive(true);
        }

        if (Input.GetKeyUp(KeyCode.Mouse1))
        {
            EndBulletTime();
            Crossair.SetActive(true);
            CrossAirAim.SetActive(false);
        }

        // Gestionar el tiempo de Bullet Time
        if (isBulletTimeActive)
        {
            bulletTimeCounter -= Time.unscaledDeltaTime;
            bulletTimeCounter = Mathf.Max(0, bulletTimeCounter);
        }
        else
        {
            if (bulletTimeCounter < bulletTimeDuration)
            {
                bulletTimeCounter += Time.unscaledDeltaTime * rechargeRate;
                bulletTimeCounter = Mathf.Min(bulletTimeCounter, bulletTimeDuration);
            }
        }

        bulletTimeSlider.value = bulletTimeCounter; // Actualizar el valor del slider

        // Finalizar el Bullet Time si se acaba el contador
        if (isBulletTimeActive && bulletTimeCounter <= 0)
        {
            EndBulletTime();
            Crossair.SetActive(true);
            CrossAirAim.SetActive(false);
        }
    }

    void StartBulletTime()
    {
        if (!isBulletTimeActive && bulletTimeCounter == bulletTimeDuration) // Solo iniciar si est� completamente recargado
        {
            isBulletTimeActive = true;
            Time.timeScale = bulletTimeScale;
        }
    }

    void EndBulletTime()
    {
        isBulletTimeActive = false;
        Time.timeScale = normalTimeScale;
    }
}