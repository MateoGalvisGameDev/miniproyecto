
using UnityEngine;
using Cinemachine;
public class AimStateManager : MonoBehaviour
{
    AimBaseState currentState;
    public HipeFireState Hip = new HipeFireState();
    public AimState Aim = new AimState();


    [SerializeField] float mouseSense = 1;
    float xAxis, yAxis;
    [SerializeField] Transform CamFollowPos;

    [HideInInspector] public Animator anim;
    [SerializeField] public CinemachineVirtualCamera VCam;
    public float adsFov = 40;
    [HideInInspector] public float hipFov;
    [HideInInspector] public float CurretnFov;
    public float fovSmoothSpeed = 10;

    public Transform aimPos;
    [HideInInspector] public Vector3 actualAimPos;
    [SerializeField] float aimSmoothSpeed = 20;
    [SerializeField] LayerMask aimMask;

    float xFollowPos;
    float yFollowPos, ogYpos;
    [SerializeField] float crouchCamHeight = 0.6f;
    [SerializeField] float shoulderSwapSpeed = 10;
    MovementStateManager moving;


    void Start()
    {
        moving = GetComponent<MovementStateManager>();
        xFollowPos = CamFollowPos.localPosition.x;
        ogYpos = CamFollowPos.localPosition.y;
        yFollowPos = ogYpos;
        hipFov = VCam.m_Lens.FieldOfView;
        anim = GetComponent<Animator>();
        SwitchState(Hip);
    }


    void Update()
    {
        xAxis += Input.GetAxisRaw("Mouse X") * mouseSense;
        yAxis -= Input.GetAxisRaw("Mouse Y") * mouseSense;
        yAxis = Mathf.Clamp(yAxis, -80, 80);


        VCam.m_Lens.FieldOfView = Mathf.Lerp(VCam.m_Lens.FieldOfView, CurretnFov, fovSmoothSpeed * Time.deltaTime);

        Vector2 screenCentre = new Vector2(Screen.width / 2, Screen.height / 2);
        Ray ray = Camera.main.ScreenPointToRay(screenCentre);

        if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, aimMask))

            aimPos.position = Vector3.Lerp(aimPos.position, hit.point, aimSmoothSpeed * Time.deltaTime);

        MoveCamera();
        currentState.UpdateState(this);
    }

    private void LateUpdate()
    {
        CamFollowPos.localEulerAngles = new Vector3(yAxis, CamFollowPos.localEulerAngles.y, CamFollowPos.localEulerAngles.z);
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, xAxis, transform.eulerAngles.z);
    }

    public void SwitchState(AimBaseState state)
    {
        currentState = state;
        currentState.EnterState(this);
    }
    void MoveCamera()
    {
        if (Input.GetKeyDown(KeyCode.LeftAlt)) xFollowPos = -xFollowPos;
        if (moving.currentState == moving.Crouching) yFollowPos = crouchCamHeight;
        else yFollowPos = ogYpos;

        Vector3 newFollowPos = new Vector3(xFollowPos, yFollowPos, CamFollowPos.localPosition.z);
        CamFollowPos.localPosition = Vector3.Lerp(CamFollowPos.localPosition, newFollowPos, shoulderSwapSpeed * Time.deltaTime);
    }
}
