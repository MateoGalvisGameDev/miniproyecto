
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] float timeToDestroy;
    [HideInInspector] public WeaponManager weapon;
    [SerializeField] GameObject impactEffectPrefab;
    [HideInInspector] public Vector3 direction;
    void Start()
    {
        Destroy(this.gameObject,timeToDestroy);
    }



    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponentInParent<EnemyHealth>()) 
        { 
            EnemyHealth enemyHealth=collision.gameObject.GetComponentInParent<EnemyHealth>();
            enemyHealth.takeDamage(weapon.damage);

            if (enemyHealth.health <= 0&&enemyHealth.isDead==false)
            {
                Rigidbody rb=collision.gameObject.GetComponentInParent<Rigidbody>();
                rb.AddForce(direction*weapon.enemyKickBackForce, ForceMode.Impulse);
                enemyHealth.isDead=true;
            }

            if (impactEffectPrefab != null)
            {
                GameObject impactEffectInstance = Instantiate(impactEffectPrefab, collision.contacts[0].point, Quaternion.LookRotation(collision.contacts[0].normal));
                Destroy(impactEffectInstance, 1f); // Destruye el efecto despu�s de 1 segundo.
            }

        }
        Destroy(this.gameObject);
    }
}
