
using UnityEngine;


public class WeaponManager : MonoBehaviour
{
    [Header("FireRate")]
    [SerializeField] float fireRate;
    [SerializeField] bool semiAuto;
    float fireRateTimer;

    [Header("Bullet Properties")]
    [SerializeField] GameObject bullet;
    [SerializeField] Transform barrelPos;
    [SerializeField] float bulletVelocity;
    [SerializeField] int bulletPerShot;
    public float damage = 20;
    AimStateManager aim;

    [SerializeField] AudioClip gunShot;
    [HideInInspector] public AudioSource audioSource;
    [HideInInspector] public WeaponAmmo ammo;
    ActionStateManager actions;
    WeaponRecoil recoil;

    [SerializeField] Light muzzleFlashlight;
    [SerializeField] ParticleSystem muzzleFlahsPacticles;
    float lightIntensity;
    [SerializeField] float lightReturnSpeed=20;

    public float enemyKickBackForce = 100;

    public Transform leftHandTarget, leftHandHint;
    public WeaponClassManager weaponClass;

    void Start()
    {
        aim= GetComponentInParent<AimStateManager>();
        actions=GetComponentInParent<ActionStateManager>();
        fireRateTimer = fireRate;  
        muzzleFlashlight.intensity = 0;
        lightIntensity = muzzleFlashlight.intensity;
  
    }

    private void OnEnable()
    {
        if (weaponClass != null)
        {
            
            ammo = GetComponent<WeaponAmmo>();
            recoil = GetComponent<WeaponRecoil>();
            audioSource = GetComponent<AudioSource>();
            recoil.recoilFollowPos = weaponClass.recoilPos;
        }

        weaponClass.SetCurrentWeapon(this);
    }
    // Update is called once per frame
    void Update()
    {
        if (ShouldFire()) fire();
        muzzleFlashlight.intensity=Mathf.Lerp(muzzleFlashlight.intensity,0,lightReturnSpeed*Time.deltaTime);
     
    }

    bool ShouldFire()
    {
        fireRateTimer += Time.deltaTime;
        if (fireRateTimer < fireRate) return false;
        if (ammo.currentAmmo==0) return false;
        if(actions.currentState==actions.Reload) return false;
        if(actions.currentState==actions.Swap) return false;
        if (semiAuto && Input.GetKeyDown(KeyCode.Mouse0)) return true;
        if (!semiAuto && Input.GetKey(KeyCode.Mouse0)) return true;
        return false;
    }

    void fire()
    {
        fireRateTimer = 0;
        barrelPos.LookAt(aim.aimPos);
        audioSource.PlayOneShot(gunShot);
        recoil.triggerRecoil();
        ammo.currentAmmo--;
        triggerMuzzleFlash();
        for (int i = 0; i < bulletPerShot; i++)
        {
            GameObject currentBullet=Instantiate(bullet,barrelPos.position,barrelPos.rotation);

            Bullet bulletScript=currentBullet.GetComponent<Bullet>();
            bulletScript.weapon = this;

            bulletScript.direction=barrelPos.transform.forward;

            Rigidbody rb=currentBullet.GetComponent<Rigidbody>();
            rb.AddForce(barrelPos.forward * bulletVelocity, ForceMode.Impulse);
        }

    }
    void triggerMuzzleFlash()
    {
        muzzleFlahsPacticles.Play();
        muzzleFlashlight.intensity = lightIntensity;
    }
}
