using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BLoquearMouse : MonoBehaviour
{
    void Update()
    {
        // Verificar si el cursor no es visible
        if (!Cursor.visible)
        {
            // Bloquear el cursor
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            // Desbloquear el cursor si es visible
            Cursor.lockState = CursorLockMode.None;
        }
    }
}