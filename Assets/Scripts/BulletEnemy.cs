using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEnemy : MonoBehaviour
{
    public float speed = 10f; // Velocidad de la bala
    public float lifetime = 2f; // Tiempo de vida de la bala
    public int damage = 10; // Da�o infligido por la bala

    void Start()
    {
        // Destruir la bala despu�s de un tiempo para evitar que se acumulen en la escena
        Destroy(gameObject, lifetime);
    }

    void Update()
    {
        // Mover la bala hacia adelante en su direcci�n
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        // Verificar si la bala ha colisionado con un objeto que tenga el tag "Player"
        if (other.CompareTag("Player"))
        {
            PlayerHealth playerHealth = other.GetComponent<PlayerHealth>();

            // Verificar si el componente PlayerHealth existe antes de usarlo
            if (playerHealth != null)
            {
                playerHealth.TakeDamage(damage); // Aplicar da�o al jugador
                Debug.Log("Jugador da�ado");
            }

            // Destruir la bala al impactar con el jugador
            Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
