using TMPro;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField] GameObject objectToEnable;
    [SerializeField] int enemyCount;
    [SerializeField] TMP_Text Text;


    public void Start()
    {
        Text.text = enemyCount.ToString();
        DisableObject();
    }

    private void Update()
    {
        Text.text = enemyCount.ToString();
    }
    public void EnemyDefeated()
    {
        
        enemyCount--;

        if (enemyCount <= 0)
        {
            EnableObject();
        }
    }

    void EnableObject()
    {
       
        objectToEnable.SetActive(true);
    }

    void DisableObject()
    {

        objectToEnable.SetActive(false);
    }
}
