using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;
public class WeaponClassManager : MonoBehaviour
{
    [SerializeField] TwoBoneIKConstraint LeftHandIK;
    public Transform recoilPos;
    ActionStateManager actions;

    public WeaponManager[] Weapons;
    int currentWeaponIndex;

    private void Awake()
    {
        currentWeaponIndex = 0;
        for (int i = 0; i < Weapons.Length; i++)
        {
            if (i==0) Weapons[i].gameObject.SetActive(true);
            else Weapons[i].gameObject.SetActive(false);
        }
    }
    public void SetCurrentWeapon(WeaponManager weapon)
    {
        if(actions == null) actions=GetComponent<ActionStateManager>();
        LeftHandIK.data.target = weapon.leftHandTarget;
        LeftHandIK.data.hint = weapon.leftHandHint;
        actions.SetWeapon(weapon);
    }

    public void ChangeWeapon(float direction)
    {
        Weapons[currentWeaponIndex].gameObject.SetActive(false);
        if (direction < 0)
        {
            if (currentWeaponIndex == 0) currentWeaponIndex = Weapons.Length - 1;
            else currentWeaponIndex--;
        }
        else
        {
            if (currentWeaponIndex == Weapons.Length - 1) currentWeaponIndex = 0;
            else currentWeaponIndex++;
        }
        Weapons[currentWeaponIndex].gameObject.SetActive(true) ;
    }

    public void WeaponPutAway()
    {
        ChangeWeapon(actions.Default.ScrollDirect);
    }
    public void WeaponPulledOu()
    {
        actions.SwitchState(actions.Default);
    }
}
