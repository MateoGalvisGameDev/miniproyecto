using TMPro;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI;

public class WeaponAmmo : MonoBehaviour
{
    public int clipSize;
    public int extraAmmo;
    [HideInInspector] public int currentAmmo;

    public TMP_Text CurrentAmmo;
    public TMP_Text ExtraAmmo;
    

    public AudioClip MagInSound;
    public AudioClip MagOutSound;
    public AudioClip releaseSlideSound;
    void Start()
    {
        currentAmmo = clipSize;
    }

    private void Update()
    {
        CurrentAmmo.text = currentAmmo.ToString();
        ExtraAmmo.text = extraAmmo.ToString();
    }


    public void Reload()
    {
        if (extraAmmo >= clipSize) 
        {
            int ammoToReload = clipSize - currentAmmo;
            extraAmmo -=ammoToReload;
            currentAmmo += ammoToReload;
        }
        else if (extraAmmo > 0)
        {
            if (extraAmmo+currentAmmo > clipSize)
            {
                int leftoverAmmo=extraAmmo+currentAmmo-clipSize;
                extraAmmo = leftoverAmmo;
                currentAmmo = clipSize;
            }
            else
            {
                currentAmmo += extraAmmo;
                extraAmmo = 0;
            }
        }
    }
}
