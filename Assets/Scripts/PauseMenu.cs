
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    bool GamePause;
    [SerializeField] GameObject PauseMenuCanvas;
    [SerializeField] GameObject Sure;
    public AimStateManager aim;
    public BulletTimeManager bulletTimeManager;

    private void Start()
    {
        Sure.SetActive(false);
        PauseMenuCanvas.SetActive(false);
        Cursor.visible = false;
        GamePause = false;
    }
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape) && !GamePause)
        {
            PauseGame();
            

        }

        else if(Input.GetKeyDown(KeyCode.Escape) && GamePause)
        {
            ResumeGame();
            
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        Cursor.visible = true;
        GamePause = true;
        PauseMenuCanvas.SetActive(true);
        bulletTimeManager.enabled = false;
        aim.enabled = false;

        Sure.SetActive(false);

    }

    public void ResumeGame()
    {
        GamePause = false;
        PauseMenuCanvas.SetActive(false);
        Time.timeScale = 1;
        Cursor.visible = false;
        aim.enabled = true;

        bulletTimeManager.enabled = true;
        
    }

    public void Restart()
    {
        Time.timeScale = 1f; 
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); 
    }

    public void Quit()
    {
        Sure.SetActive(true);
        PauseMenuCanvas.SetActive(false);
    }

    public void ConfirmQuit()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
