using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Empujar : MonoBehaviour
{
    public float fuerzaEmpuje = 10f;
    public GameObject ragdoll;
    private void Start()
    {
        Cursor.visible = true;
        EmpujarRagdollConFuerza();
    }

    void EmpujarRagdollConFuerza()
    {
        // Buscar el objeto ragdoll en la escena (ajusta el nombre del objeto seg�n tu configuraci�n)
       

        if (ragdoll != null)
        {
            // Obtener la direcci�n desde el objeto actual al ragdoll
            Vector3 direccionEmpuje = ragdoll.transform.position - transform.position;

            // Normalizar la direcci�n para mantener solo la direcci�n
            direccionEmpuje.Normalize();

            // Aplicar la fuerza al ragdoll
            Rigidbody[] ragdollRigidbodies = ragdoll.GetComponentsInChildren<Rigidbody>();
            foreach (Rigidbody rb in ragdollRigidbodies)
            {
                rb.AddForce(direccionEmpuje * fuerzaEmpuje, ForceMode.Impulse);
            }
        }
        else
        {
            Debug.LogError("No se encontr� el objeto ragdoll. Aseg�rate de que el nombre sea correcto.");
        }
    }
}
