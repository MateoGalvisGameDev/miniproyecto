
using UnityEngine;

public class AnimationEnter : MonoBehaviour
{
    [SerializeField] Animation animation01;
    [SerializeField]Animation animation02;

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //HACE ANIMACION01
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //HACE ANIMACION02
        }

    }
}