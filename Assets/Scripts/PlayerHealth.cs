using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine;


public class PlayerHealth : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth;
    public TMP_Text Salud;
    public MovementStateManager movementStateManager;
    public AimStateManager aimStateManager;
    public string SceneToLoad;

    [SerializeField] AudioClip Dead;
    [SerializeField] AudioClip Hit;
    public AudioSource audioSource;


    public RagdollManager ragdollManager;
    public HealthBar healthBar; // A�ade esta l�nea

    private void Start()
    {
        currentHealth = maxHealth;
        healthBar.slider.maxValue = maxHealth; // Actualiza el valor m�ximo de la barra de vida
        healthBar.slider.value = currentHealth; // Actualiza el valor actual de la barra de vida
        Salud.text= currentHealth.ToString()+" / "+maxHealth.ToString();
    }

    private void Update()
    {
        Salud.text = currentHealth.ToString() + " / " + maxHealth.ToString();
    }

    public void TakeDamage(int damage)
    {
        audioSource.PlayOneShot(Hit);
        currentHealth -= damage;
        healthBar.slider.value = currentHealth; // Actualiza la barra de vida cuando el jugador recibe da�o

        if (currentHealth <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        movementStateManager.enabled = false;
        aimStateManager.enabled = false;
        ragdollManager.TriggerRadoll();
        Debug.Log("El jugador ha muerto.");
        audioSource.PlayOneShot(Dead);
        audioSource.enabled = false;

        // Reinicia la escena despu�s de 5 segundos
        Invoke("ReiniciarEscena", 2f);
    }

    private void ReiniciarEscena()
    {
        // Puedes reiniciar la escena carg�ndola nuevamente
        SceneManager.LoadScene(SceneToLoad);
    }
}
