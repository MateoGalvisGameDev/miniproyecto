using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeSceneOnEnter : MonoBehaviour
{
    public string sceneToLoad;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            // Carga la escena
            SceneManager.LoadScene(sceneToLoad);
            Debug.Log("Cambiar Escena a " + sceneToLoad);

        }

     
    }
}
