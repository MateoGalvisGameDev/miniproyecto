using UnityEngine.Animations.Rigging;
using UnityEngine;

public class RagdollManager : MonoBehaviour
{
    public Rig Rig;
    Rigidbody[] rbs;
    public Animator animator;
    private void Start()
    {
        rbs = GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rb in rbs) rb.isKinematic = true;
    }

    public void TriggerRadoll()
    {
        animator.enabled = false;
        Rig.weight = 0;
        foreach (Rigidbody rb in rbs) rb.isKinematic = false;
    }
}

