
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.ProBuilder;

public class PlayerChase : MonoBehaviour
{
    public Transform jugador;
    public NavMeshAgent navMeshAgent;
    public Animator animator; // Referencia al Animator del enemigo
    public EnemyShooting enemyShooting; // Referencia al script de disparo del enemigo
    public LayerMask capasEvitar;
    public float distanciaFrenado = 1.5f; // Distancia para detener la persecuci�n
    public float distanciaDisparo = 3.0f; // Distancia para iniciar el disparo

    private float tiempoDesdeUltimoDisparo = 0f;
    public float tiempoEntreDisparos = 1f;

    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>(); // Asegura que el componente Animator est� asignado
    }

    void Update()
    {
        if (jugador != null && navMeshAgent != null)
        {
            Vector3 direccionJugador = jugador.position - transform.position;
            RaycastHit hit;
            // Comprobamos si hay l�nea de visi�n con el jugador sin obst�culos
            if (!Physics.Raycast(transform.position, direccionJugador, out hit, Mathf.Infinity, capasEvitar))
            {
                float distanciaJugador = direccionJugador.magnitude;
                if (distanciaJugador > distanciaFrenado)
                {
                    // Perseguir al jugador
                    navMeshAgent.SetDestination(jugador.position);
                    animator.SetBool("Shooting", false);
                }
                else
                {
                    // Detenerse y orientar hacia el jugador
                    navMeshAgent.ResetPath();
                    Quaternion rotacionObjetivo = Quaternion.LookRotation(direccionJugador.normalized);
                    transform.rotation = Quaternion.Slerp(transform.rotation, rotacionObjetivo, Time.deltaTime * navMeshAgent.angularSpeed);

                    // Decidir si disparar basado en la distancia y el tiempo entre disparos
                    if (distanciaJugador <= distanciaDisparo)
                    {
                        animator.SetBool("Shooting", true); // Activar animaci�n de disparo
                        tiempoDesdeUltimoDisparo += Time.deltaTime;
                        Debug.Log("Intentando disparar, tiempo desde �ltimo disparo: " + tiempoDesdeUltimoDisparo);
                        if (tiempoDesdeUltimoDisparo >= tiempoEntreDisparos)
                        {
                            Debug.Log("Disparando");
                            enemyShooting.Shoot(); // Disparar
                            tiempoDesdeUltimoDisparo = 0f; // Reiniciar el contador de tiempo entre disparos
                        }
                    }
                    else
                    {
                        animator.SetBool("Shooting", false);
                    }
                }
            }
        }
    }
}
    
