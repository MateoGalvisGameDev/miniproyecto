using UnityEngine.Animations.Rigging;
using UnityEngine;
using UnityEngine.AI;

public class EnemyStateManager : MonoBehaviour
{
    public EnemyMovement enemyMovement;
    public PlayerDetected playerDetected;
    public PlayerChase chase;
    public EnemyShooting shooting;
    public Animator animator;
    public NavMeshAgent navMeshAgent;
    public float shootInterval = 5f;

    public enum Estado
    {
        Patrullaje,
        Alerta,
        Ataque,
        Dead
    }

    public Estado estadoActual;
    private float shootTimer = 0f;

    void Start()
    {
        CambiarEstado(Estado.Patrullaje);
    }

    void Update()
    {
        UpdateAnimationState();  // Actualiza las animaciones basadas en la velocidad

    }

    private void UpdateAnimationState()
    {
        float speed = navMeshAgent.velocity.magnitude;
        float chaseSpeedThreshold = 3.5f; // Define el umbral de velocidad para correr
        float walkSpeedThreshold = 0.1f;  // Define el umbral de velocidad para caminar

        if (speed < walkSpeedThreshold)
        {
            animator.SetBool("Looking", true);
            animator.SetBool("Walking", false);
            animator.SetBool("Chasing", false);
        }
        else if (speed < chaseSpeedThreshold)
        {
            animator.SetBool("Looking", false);
            animator.SetBool("Walking", true);
            animator.SetBool("Chasing", false);
        } 
        else if(animator.GetBool("Shooting"))
        {
            animator.SetBool("Looking", true);
            animator.SetBool("Walking", false);
            animator.SetBool("Chasing", false);
        }
        else
        {
            animator.SetBool("Looking", false);
            animator.SetBool("Walking", false);
            animator.SetBool("Chasing", true);
        }
    }

    // Implementación de los métodos de estado (Patrullaje, Alerta, Ataque, Dead)...

    public void CambiarEstado(Estado nuevoEstado)
    {
        estadoActual = nuevoEstado;
        shootTimer = 0f;

        if (nuevoEstado != Estado.Ataque && nuevoEstado != Estado.Dead)
        {
            navMeshAgent.isStopped = false;
        }
    }
}
