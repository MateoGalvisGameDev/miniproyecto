using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour
{
    public Transform firePoint; // Punto de origen del disparo
    public GameObject bulletPrefab; // Prefab de la bala
    public float bulletSpeed = 10f; // Velocidad de la bala
    public EnemyStateManager enemyStateManager;
    public AudioSource audio;
    public AudioClip shoot;
    public Transform jugador; // Referencia al transform del jugador

    public void Shoot()
    {
        // Verifica si est� en el estado de ataque antes de disparar
       
            audio.PlayOneShot(shoot);
            Debug.Log("Disparo desde EnemyShooting");
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            if (rb != null)
            {
                // Calcular la direcci�n hacia el jugador, teniendo en cuenta su movimiento
                Vector3 direccionDisparo = CalcularDireccionDisparo(jugador);
                rb.velocity = direccionDisparo * bulletSpeed;
            }
    }

    private Vector3 CalcularDireccionDisparo(Transform objetivo)
    {
        // Obtener la posici�n actual y la velocidad del jugador (si es posible)
        Vector3 posicionJugador = objetivo.position;
        Vector3 velocidadJugador = objetivo.GetComponent<Rigidbody>()?.velocity ?? Vector3.zero;

        // Calcular el tiempo que tardar� la bala en llegar al jugador
        float distancia = Vector3.Distance(firePoint.position, posicionJugador);
        float tiempoLlegada = distancia / bulletSpeed;

        // Predecir la posici�n futura del jugador
        Vector3 posicionFutura = posicionJugador + (velocidadJugador * tiempoLlegada);

        // Calcular la direcci�n hacia la posici�n futura del jugador
        Vector3 direccionDisparo = (posicionFutura - firePoint.position).normalized;

        return direccionDisparo;
    }

    private void Update()
    {
        if (jugador != null)
        {
            Vector3 direccionJugador = jugador.position - transform.position;
            Quaternion rotacionObjetivo = Quaternion.LookRotation(direccionJugador);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotacionObjetivo, Time.deltaTime * 5f);
        }
    }
}
