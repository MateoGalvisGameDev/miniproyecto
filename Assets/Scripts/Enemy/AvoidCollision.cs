
using UnityEngine;
using UnityEngine.AI;

public class AvoidCollision : MonoBehaviour
{
    public NavMeshObstacle navMeshObstacle;

    void Start()
    {
        navMeshObstacle = GetComponent<NavMeshObstacle>();
    }

    void Update()
    {
        // Activamos el NavMeshObstacle para evitar colisiones
        navMeshObstacle.enabled = true;

        // Comprobamos si hay colisiones con otros obstáculos
        Collider[] colliders = Physics.OverlapSphere(transform.position, 2f);
        foreach (Collider collider in colliders)
        {
            if (collider != gameObject && collider.GetComponent<NavMeshObstacle>() != null)
            {
                navMeshObstacle.enabled = false;
                return;
            }
        }
    }
}
