
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Animator animator;  // Referencia al Animator

    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();  // Aseg�rate de tener el componente Animator
        EstablecerDestinoAleatorio();
    }

    void Update()
    {
        // Si el enemigo ha llegado a su destino, establecemos un nuevo destino aleatorio
        if (!navMeshAgent.pathPending && navMeshAgent.remainingDistance < 0.1f)
        {
            EstablecerDestinoAleatorio();
        }
    }

    void EstablecerDestinoAleatorio()
    {
        // Generamos un destino aleatorio en el NavMesh
        Vector3 destinoAleatorio = RandomNavMeshLocation(10f);
        navMeshAgent.SetDestination(destinoAleatorio);
    }

    Vector3 RandomNavMeshLocation(float radius)
    {
        Vector3 randomDirection = Random.insideUnitSphere * radius;
        randomDirection += transform.position;
        NavMeshHit hit;
        NavMesh.SamplePosition(randomDirection, out hit, radius, NavMesh.AllAreas);
        return hit.position;
    }
}
