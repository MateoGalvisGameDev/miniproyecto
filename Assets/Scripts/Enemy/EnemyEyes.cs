
using UnityEngine;
using static EnemyStateManager;

public class EnemyEyes : MonoBehaviour
{
    public EnemyStateManager enemyStateManager; // Referencia al script del enemigo
    public GameObject ojos; // Objeto desde el cual lanzaremos el raycast
    public float distanciaMaxima = 10f; // Distancia m�xima del raycast
    public LayerMask capasImpacto; // Capas a considerar para la detecci�n

    void Update()
    {
        // Lanzar un raycast desde el objeto "ojos" en direcci�n al jugador
        RaycastHit hit;
        if (Physics.Raycast(ojos.transform.position, ojos.transform.forward, out hit, distanciaMaxima, capasImpacto))
        {
            // Verificar si el raycast ha impactado con el jugador
            if (hit.collider.CompareTag("Player"))
            {
                // Cambiar el estado del enemigo a Alerta
                enemyStateManager.CambiarEstado(Estado.Ataque); 
            }
            else
            {
                enemyStateManager.CambiarEstado(Estado.Alerta);
            }
        }
    }
}
