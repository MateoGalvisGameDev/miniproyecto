using UnityEngine.UI;
using UnityEngine;
using static EnemyStateManager;

public class EnemyHealth : MonoBehaviour
{
    public Image targetImage;
    public Image targetImage2;
    public Color newColor;
    public float health;
    RagdollManager ragdollManager;
    [HideInInspector] public bool isDead;
    [SerializeField] LevelManager levelManager;
    public EnemyStateManager enemyStateManager;
    public EnemyShooting shooting;
    public Animator anim;
    public AudioClip Hit;
    public AudioSource audioSource;
    public GameObject objetoSoltado;
    private void Start()
    {
        ragdollManager = GetComponent<RagdollManager>();
    }
    public void takeDamage(float damage)
    {
        if (health > 0)
        {
            health -= damage;
            if (health <= 0) EnemyDeath();
            else
            {
                audioSource.PlayOneShot(Hit);
                Debug.Log("hit");
            }
        }
        ChangeColor();
    }

    void EnemyDeath()
    {
        anim.enabled = false;
        ragdollManager.TriggerRadoll();
        levelManager.EnemyDefeated();
        Debug.Log("Death");
        shooting.enabled = false;
        enemyStateManager.CambiarEstado(Estado.Dead);

        // Teleportar el objeto presente en la escena a la posici�n del enemigo
        if (objetoSoltado != null)
        {
            objetoSoltado.transform.position = transform.position;
        }

        Destroy(this.gameObject, 0.5f);

    }

    public void ChangeColor()
    {
        targetImage.color = Color.red;
        targetImage2.color = Color.red;
        Invoke("ChangeToWhite", 1f);
    }
    void ChangeToWhite()
    {
        targetImage.color = Color.white;
        targetImage2.color = Color.white;
    }
}


