
using UnityEngine;
using static EnemyStateManager;

public class PlayerDetected : MonoBehaviour
{
    public float radioDeteccion = 5f;
    public LayerMask capasJugador;
    public EnemyStateManager enemyStateManager;

    private void Update()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, radioDeteccion);
        foreach (Collider collider in colliders)
        {
            // Verificar si el collider pertenece al jugador
            if (collider.CompareTag("Player"))
            {
                enemyStateManager.CambiarEstado(Estado.Alerta);
                break; // Salir del bucle una vez que se ha detectado al jugador
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        // Dibujar un gizmo visualizando el radio de detecci�n en el editor
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radioDeteccion);
    }
}
