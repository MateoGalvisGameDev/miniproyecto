using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class DefaultState : ActionBaseState
{
    public float ScrollDirect;
    public override void EnterState(ActionStateManager actions)
    {
        actions.rhandAim.weight = 1;
        actions.lHandI.weight = 1;
    }
    public override void UpdateState(ActionStateManager actions)
    {
        actions.rhandAim.weight = Mathf.Lerp(actions.rhandAim.weight, 1, 5 * Time.deltaTime);
        if(actions.lHandI.weight ==0) actions.lHandI.weight = 1;

        if (Input.GetKeyDown(KeyCode.R)&&CanReload(actions)) 
        {
            actions.SwitchState(actions.Reload);    
        }
        else if(Input.mouseScrollDelta.y !=0)
        {
            ScrollDirect=Input.mouseScrollDelta.y;
            actions.SwitchState(actions.Swap);
        }
    }
    bool CanReload(ActionStateManager action) 
    {
        if (action.ammo.currentAmmo == action.ammo.clipSize) return false;
        else if (action.ammo.extraAmmo == 0) return false;
        else return true;
    }
}
